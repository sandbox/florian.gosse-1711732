<?php

class EntityTeaser {
  
  protected $machine_name;
  protected $name;
  protected $configuration;

  protected function __construct( $machine_name = '', $name = '', object $configuration = NULL ) {
    $this->setMachineName( $machine_name );
    $this->setName( EntityTeaser::returnName( $machine_name, $name ) );
    if( !empty( $configuration ) ) {
      $this->setConfiguration( EntityTeaserConfiguration::validConfiguration( $configuration ) ); 
    } else {
      $this->setConfiguration( new EntityTeaserConfiguration() );
    }
  }

  public function getMachineName() {
    return $this->machine_name;
  }

  public function getName() {
    return $this->name;
  }

  public function getConfiguration() {
    return $this->configuration;
  }


  public function setMachineName( $machine_name ) {
    $this->machine_name = $machine_name;
  }

  public function setName( $name ) {
    if( is_string( $name ) ) {
      $this->name = check_plain( $name );
    } else {
      throw new Exception( t("Name must be a string."), 1);
    }
  }

  public function setConfiguration( EntityTeaserConfiguration $configuration ) {
    $this->configuration = $configuration;
    // $this->configuration = EntityTeaserConfiguration::validConfiguration( $configuration );
  }

  public function hasType( $type ) {
    $config = $this->configuration;
    if( array_key_exists( $type, $config->getTypes() ) ) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  public static function returnName( $machine_name, $name = '' ) {
    if( empty( $name ) && !empty($machine_name) ) {
      return $machine_name;
    } else {
      return $name;
    }
  }

}
