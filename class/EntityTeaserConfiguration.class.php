<?php

class EntityTeaserConfiguration {
  
  private $config_id;

  /**
   * E.g.:
   *  array( 'node', 'user' )
   */
  private $types;

  /**
   * E.g.:
   *  array( 'node' => array( 'article', 'page' ) )
   */
  private $bundles;


  public function __construct( $config_id = 0, array $types = array(), array $bundles = array() ) {
    $this->config_id = $config_id;
    $this->types = $types;
    $this->bundles = $bundles;
  }

  public function getConfigId() {
    return $this->config_id;
  }

  public function getTypes() {
    return $this->types;
  }

  public function getTypesFull() {
    $types = array();
    $entity_info = self::getEntityTypes();
    if( !empty( $this->types ) ) {
      foreach ( $this->types as $type ) {
        if( isset( $entity_info[$type] ) ) {
          $types[$type] = $entity_info[$type];
          if( isset( $this->bundles[$type] ) && !empty( $this->bundles[$type] ) ) {
            foreach ($types[$type]['bundles'] as $bundle => $data) {
              if( !in_array( $bundle , $this->bundles[$type] ) ) {
                unset( $types[$type]['bundles'][$bundle] );
              }
            }
            ksort($types[$type]['bundles']);
          } else {
            $types[$type]['bundles'] = array();
          }
        }
      }
      ksort( $types );
      return $types;
    } else {
      return array();
    }
  }

  public function getBundles() {
    return $this->bundles;
  }

  public function getTypeBundles( $type ) {
    if( array_key_exists( $type , $this->bundles ) ) {
      return $this->bundles[$type];
    }
  }

  public function setConfigId( $config_id ) {
    $this->config_id = $config_id;
  }

  public function setTypes( array $types ) {
    $this->types = $types;
  }

  public function setBundles( array $bundles ) {
    $this->bundles = $bundles;
  }

  public function setTypeBundles( $type, array $bundles ) {
    if( in_array( $type , $this->types ) ) {
      if( !is_array($this->bundles) ) {
        $this->bundles = array();
      }
      $this->bundles[$type] = $bundles;
    }
  }

  public function valid() {
    $valid = TRUE;
    if( empty( $this->types ) || !is_array( $this->types ) ) {
      $valid = FALSE;
    }
    return $valid;
  }

  public function save() {
    if( !$this->valid() ) {
      return FALSE;
    }
    // Check if configuration existis or will saved first time.
    if( empty( $this->config_id ) ) {
      // Insert new configuration.
      $config_id = db_insert( 'entity_teaser_configuration' )
        ->fields( array(
            'types',
            'bundles',
          ) )
        ->values( array(
            'types' => serialize( $this->types ),
            'bundles' => serialize( $this->bundles ),
          ) )
        ->execute();
      $this->config_id = $config_id;
    } else {
      // Update exisiting configuration by config id.
      db_update( 'entity_teaser_configuration' )
        ->fields( array(
            'types' => serialize( $this->types ),
            'bundles' => serialize( $this->bundles ),
          ) )
        ->condition( 'config_id', $this->config_id )
        ->execute();
    }
  }

  public function load() {
    if( !empty($this->config_id) ) {
      $result = db_select( 'entity_teaser_configuration', 'etc' )
        ->fields( 'etc' )
        ->condition( 'etc.config_id', $this->config_id )
        ->execute();
      while( $record = $result->fetchObject() ) {
        $this->types = unserialize( $record->types );
        $this->bundles = unserialize( $record->bundles );
        continue;
      }
    } else {
      return FALSE;
    }
  }

  public function delete() {
    if( !empty($this->config_id) ) {
      db_delete( 'entity_teaser_configuration' )
        ->condition( 'config_id', $this->config_id )
        ->execute();
      return TRUE;
    } else {
      return FALSE;
    }
  }

  public static function getEntityTypes() {
    $entity_info = module_invoke_all( 'entity_info' );
    foreach ($entity_info as $type => &$info) {
      if( !isset( $info['entity keys']['bundle'] ) ) {
        $info['bundles'] = array();
      }
      drupal_alter( 'entity_teaser_entity_keys', $type, $info['entity keys'] );
    }
    ksort( $entity_info );
    return $entity_info;
  }

  public static function validConfiguration( $configuration ) {
    if( $configuration instanceof self ) {
      return $configuration;
    } else {
      throw new Exception( t("Wrong argument is set. Must be an object of type 'EntityTeaserConfiguration'."), 1);
    }
  }

  public static function validTypes( array $types ) {
    $entity_info = self::getEntityTypes();
    $types = array();
    if( !empty( $types ) ) {
      foreach ( $types as $index => $type ) {
        if( !isset( $entity_info[$value] ) ) {
          unset($types[$index]);
          drupal_set_message( t('The type @type is not valid.', array( '@type' => $type ) ), 'warning' );
        }
      }
    }
  }

}
