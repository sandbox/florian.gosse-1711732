<?php

class EntityTeaserBlock extends EntityTeaser {

  // Displayed attributes
  private $title;
  private $type;
  private $cid;

  public function __construct( $machine_name = '', $name = '', object $configuration = NULL, $title = '', $type = '', $cid = 0 ) {
    parent::__construct( $machine_name, $name, $configuration );
    $this->title = $title;
    $this->type = $type;
    $this->cid = $cid;
  }

  public function getTitle() {
    return $this->title;
  }

  public function getType() {
    return $this->type;
  }

  public function getCid() {
    return $this->cid;
  }


  public function setTitle( $title ) {
    $this->title = $title;
  }

  public function setType( $type ) {
    $this->type = $type;
  }

  public function setCid( $cid ) {
    $this->cid = $cid;
  }

  public function hasSet() {
    // @TODO implement query for check if block is used in a set
    return false;
  }

  public function hasOwnConfig() {

    // Return static until the entity_teaser set implemented
    return TRUE;
  }

  public function save( $save_mode = ENTITY_TEASER_SAVE_MODE_ALL ) {

    if( $save_mode === ENTITY_TEASER_SAVE_MODE_ADMINISTRATIV || $save_mode === ENTITY_TEASER_SAVE_MODE_ALL ) {
      if( $this->hasOwnConfig() ) {
        $this->configuration->save();
      }
    }

    if( !EntityTeaserBlock::exists( $this->machine_name ) ) {
      db_insert( 'entity_teaser_block' )
        ->fields( array( 
            'machine_name' => $this->machine_name,
            'name' => $this->name,
            'config' => $this->configuration->getConfigId(),
            'title' => $this->title,
            'type' => $this->type,
            'cid' => $this->cid,
          ) )
        ->execute();
    } else {
      $values = array();
      if( $save_mode === ENTITY_TEASER_SAVE_MODE_CONTENT || $save_mode === ENTITY_TEASER_SAVE_MODE_ALL ) {
        $values['title'] = $this->title;
        $values['type'] = $this->type;
        $values['cid'] = $this->cid;
      }
      if( $save_mode === ENTITY_TEASER_SAVE_MODE_ADMINISTRATIV || $save_mode === ENTITY_TEASER_SAVE_MODE_ALL ) {
        $config = $this->configuration;
        $values['name'] = $this->name;
        $values['config'] = $config->getConfigId();
      }
      db_update( 'entity_teaser_block' )
        ->fields( $values )
        ->condition( 'machine_name', $this->machine_name )
        ->execute();
    }
  }

  public function delete() {
    $this->load();
    db_delete( 'block' )
      ->condition( 'module', 'entity_teaser' )
      ->condition(
        db_or()
          ->condition( 'delta', ENTITY_TEASER_BLOCK_BLOCK_PREFIX.$this->machine_name )
          ->condition( 'delta', ENTITY_TEASER_BLOCK_CONFIG_PREFIX.$this->machine_name )
        )
      ->execute();

    $result = db_select( 'entity_teaser_block', 'tb' )
      ->fields( 'tb' )
      ->condition( 'config', $this->configuration->getConfigId() )
      ->execute();
    if( $result->rowCount() > 0 ) {
      $this->configuration->delete();
    }

    db_delete( 'entity_teaser_block' )
      ->condition( 'machine_name', $this->machine_name )
      ->execute();
  }

  public function load() {
    $result = db_select( 'entity_teaser_block', 'tb' )
      ->fields( 'tb' )
      ->condition( 'tb.machine_name', $this->machine_name )
      ->execute();
    while ( $obj = $result->fetchObject() ) {
      $this->machine_name = $obj->machine_name;
      $this->name = $obj->name;
      $this->configuration = new EntityTeaserConfiguration( $obj->config );
      $this->configuration->load();
      $this->title = $obj->title;
      $this->type = $obj->type;
      $this->cid = $obj->cid;
      break;
    }
  }

  public function view() {
    $entity_info = $this->configuration->getTypesFull();
    if( isset( $entity_info[ $this->type ] ) ) {
      $label = $entity_info[ $this->type ][ 'entity keys' ][ 'label' ];
      $entity = entity_load( $this->type, array( $this->cid ), array(), TRUE );
      if( !empty( $this->title ) ) {
        $entity[$this->cid]->{$label} = $this->title;
      }
      $render_array = entity_view( $this->type, $entity, 'entity_teaser_block' );
      foreach ($render_array[ $this->type ] as $id => &$object ) {
        if( is_int( $id ) ) {
          if( array_key_exists( '#contextual_links', $object ) ) {
            unset( $object['#contextual_links'] );
          }
        }
      }
      return $render_array;
    } else {
      return FALSE;
    }
  }

  public function hasContent() {
    if( empty($this->type) || empty($this->cid) ) {
      return FALSE;
    } else {
      return TRUE;
    }
  }

  public static function isValidContent( $type, $cid ) {
    if( !is_int( $cid ) || !is_string( $type ) ) {
      return FALSE;
    }
    $entity = entity_load( $type, array( $cid ), array(), TRUE );
    if( !empty($entity) ) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  public static function loadAll() {
    $entity_teaser_blocks = array();

    $result = db_select( 'entity_teaser_block', 'tb' )
      ->fields( 'tb', array( 'machine_name' ) )
      ->execute();
    while ( $machine_name = $result->fetchField() ) {
      $entity_teaser_block = new EntityTeaserBlock( $machine_name );
      $entity_teaser_block->load();
      $entity_teaser_blocks[ $entity_teaser_block->getMachineName() ] = $entity_teaser_block;
    }
    return $entity_teaser_blocks;
  }

  public static function exists( $machine_name ) {
    $count = db_select( 'entity_teaser_block', 'tb' )
      ->fields( 'tb' )
      ->condition( 'tb.machine_name', $machine_name )
      ->execute()
      ->rowCount();

    if( $count > 0 ) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

}
