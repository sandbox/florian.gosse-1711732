<?php

/**
 * Provide administrativ overview.
 */
function entity_teaser_config_overview() {
  $output = "";
  $output .= '<div id="entity-teaser-area-block">';
    $output .= "<h1>".t('Blocks')."</h1>";
    $output .= "<p>".l( t('Go to full blocks overview'), 'admin/structure/entity_teaser/block' )."<p>";
    $output .= entity_teaser_block_block_table();
  $output .= "</div>";

  $output .= '<div id="entity-teaser-area-set">';
    $output .= "<h1>".t('Sets')."</h1>";
    $output .= "<p>".t('Sets are not support in the actual development status.')."</p>";
  $output .= "</div>";

  return $output;
}


/**
 * Provide administrativ entity_teaser block overview.
 */
function entity_teaser_block_config_overview() {
  $output = "";

  $output .= entity_teaser_block_block_table();

  return $output;
}

/**
 *  Return table which list all entity_teaser blocks.
 */
function entity_teaser_block_block_table() {
  $table = array(
    'header' => array(
      array(
        'data' => t('Name')
      ),
      array(
        'data' => t('Machine name')
      ),
      array(
        'data' => t('Operation'),
        'colspan' => 3
      ),
    )
  );
  $rows = array();
  $entity_teaser_block = EntityTeaserBlock::loadAll();
  foreach ($entity_teaser_block as $machine_name => $entity_teaser_block) {
    $rows[] = array(
      $entity_teaser_block->getName(),
      $entity_teaser_block->getMachineName(),
      l( t('Edit'), 'admin/structure/entity_teaser/block/edit/'.$entity_teaser_block->getMachineName() ),
      l( t('Delete'), 'admin/structure/entity_teaser/block/delete/'.$entity_teaser_block->getMachineName() ),
      l( t('Clone'), 'admin/structure/entity_teaser/block/clone/'.$entity_teaser_block->getMachineName() ),
    );
  }

  $table['rows'] = $rows;
  $table['attributes'] = array();
  $table['caption'] = '';
  $table['colgroups'] = array(

  );
  $table['sticky'] = TRUE;
  $table['empty'] = t('No Blocks available. <a href="@createblock">Create</a> new one.', array(
    '@createblock' => url('admin/structure/entity_teaser/block/add', array( 
      'query' => array( 
        'destination' => current_path() 
      ) 
    ) ) 
  ) );

  return theme_table( $table );
}

/**
 * The entity_teaser block config form.
 */
function entity_teaser_block_config_form( $form, &$form_state, $op = 'add', $block = null ) {
  $form = array();

  // Set operation type
  $form_state['op'] = $op;

  // If operation is 'add' create a new Entity Teaser block.
  if( empty($block) || $op === 'add' ) {
    $block = new EntityTeaserBlock('');
  }
  
  $form_state['entity_teaser_block'] = &$block;

  // Add fields for set administrative and machine name
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Administrative Title'),
    '#default_value' => $block->getName(),
    '#required' => TRUE,
    '#maxlength' => 255,
    '#weight' => 0,
  );
  $form['machine_name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Machine name'),
    '#default_value' => $block->getMachineName(),
    '#required' => TRUE,
    // the length is the difference max delta length for blocks and the entity_teaser block prefix
    '#maxlength' => 29,
    '#weight' => 5,
    '#machine_name' => array(
      'exists' => 'entity_teaser_block_machine_name_exists',
      'source' => array('name'),
    ),
  );

  if( $op === 'edit' ) {
    $form['machine_name']['#disabled'] = TRUE;
  }

  // Create fieldset for entity type settings
  $form['entity_configuration'] = array(
    '#type' => 'fieldset',
    '#title' => t('Configuration'),
    '#weight' => 10,
  );

  // Add container for theme the configuration form
  $form['entity_configuration']['container_left'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'container',
        'container-left'
      ),
    ),
    '#weight' => 15,
  );
  $form['entity_configuration']['container_right'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'container',
        'container-right'
      ),
    ),
    '#weight' => 20,
  );
  // Add clearfix
  $form['entity_configuration']['container_clearfix'] = array(
    '#markup' => '<div class="clearfix"></div>',
    '#weight' => 25,
  );

  // Fetch all usable entity types
  $entity_types = EntityTeaserConfiguration::getEntityTypes();
  $etypes_options = array();

  // Get all selected types of entity teaser block
  $default_value_etypes = $block->getConfiguration()->getTypes();

  // Set weight for right order of fieldsets
  $weight = 35;

  foreach ( $entity_types as $type => $data) {
    // Add entity types to options array
    $etypes[$type] = $data['label'];

    // Check if type has bundles
    $bundles = $entity_types[$type]['bundles'];
    if( !empty( $bundles ) ) {
      // Create fieldset for every type which has bundles
      $form['entity_configuration']['container_right'][ 'bundles_configuration_'.$type ] = array(
        '#type' => 'fieldset',
        '#title' => t('<em>@type</em> options', array( '@type' => $data['label'] ) ),
        '#weight' => $weight,
        '#states' => array(
          'visible' => array( ':input[name="config_etypes['.$type.']"]' => array( 'checked' => TRUE ) ),
        ),
      );

      // Attach all bundles of type to bundles options array
      $bundles_options = array();
      foreach ($bundles as $bundles_type => $info) {
        $bundles_options[$bundles_type] = $info['label'];
      }
      // Add checkbox for bundles selection
      $form['entity_configuration']['container_right'][ 'bundles_configuration_'.$type ][ 'bundles_'.$type ] = array(
        '#type' => 'checkboxes',
        '#title' => t('Bundles'),
        '#options' => $bundles_options,
      );

      // Check if entity teaser block has selected bundles of type
      $default_value = $block->getConfiguration()->getTypeBundles( $type );
      if( !empty($default_value) ) {
        // If type has any bundle add default options
        $form['entity_configuration']['container_right'][ 'bundles_configuration_'.$type ][ 'bundles_'.$type ][ '#default_value' ] = $default_value;
      }

      // Increment weight for next fieldset
      $weight = $weight+5;
    }
    
  }

  // Checkboxes form element for available entity types
  $form['entity_configuration']['container_left']['config_etypes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Entity types'),
    '#options' => $etypes,
    '#default_value' => $default_value_etypes,
    '#required' => TRUE,
    '#weight' => 29,
  );

  // Attach submit button
  $form['actions'] = array(
    '#type' => 'actions'
  );
  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  // Attach validate and submit handler
  $form['#validate'][] = 'entity_teaser_block_config_form_validate';
  $form['#submit'][] = 'entity_teaser_block_config_form_submit';

  return $form;  
}

/**
 * Handler for check if the machine_name already exists.
 */
function entity_teaser_block_machine_name_exists( $machine_name ) {
  return EntityTeaserBlock::exists( $machine_name );
}

/**
 * Provide validate handler for block configuration form.
 */
function entity_teaser_block_config_form_validate( $form, &$form_state ) {

}

/**
 * Provide submit handler for block configuration form.
 */
function entity_teaser_block_config_form_submit( $form, &$form_state ) {
  $form_state['redirect'] = 'admin/structure/entity_teaser/block';

  $op = $form_state['op'];
  $block = &$form_state['entity_teaser_block'];

  $value = &$form_state['input'];

  $types = array();
  $bundles = array();

  foreach ( $value['config_etypes'] as $type => $name ) {
    if( !empty( $name ) && $type === $name ) {
      $types[] = $type;
      if( isset( $value['bundles_'.$type] ) ) {
        foreach ( $value['bundles_'.$type] as $bundle => $bundle_name ) {
          if( !empty( $bundle_name ) && $bundle === $bundle_name ) {
            $bundles[$type][] = $bundle;
          }
        }
      }
    }
  }
  
  $config = $block->getConfiguration();
  $config->setTypes( $types );
  $config->setBundles( $bundles );

  $block->setConfiguration( $config );
  if( $op === 'add' ) {
    $block->setMachineName( $value['machine_name'] );
  }
  $block->setName( $value['name'] );
  $block->save( ENTITY_TEASER_SAVE_MODE_ADMINISTRATIV );
  if( $op === 'add' ) {
    drupal_set_message( t( 'Entity Teaser block <em>@blockname</em> has been created successful.', array( '@blockname' => $block->getName() ) ) );
  } elseif( $op === 'edit' ) {
    drupal_set_message( t( 'Entity Teaser block <em>@blockname</em> has been updated successful.', array( '@blockname' => $block->getName() ) ) );
  }
  

}

/**
 * The entity_teaser block clone form.
 */
function entity_teaser_block_clone_form( $form, &$form_state, $block ) {
  $form = array();

  $form_state['entity_teaser_block'] = &$block;

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Administrative Title'),
    '#default_value' => '',
    '#required' => TRUE,
    '#maxlength' => 255,
    '#weight' => 0,
  );

  $form['machine_name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Machine name'),
    '#default_value' => '',
    '#required' => TRUE,
    // the length is the difference max delta length for blocks and the entity_teaser block prefix
    '#maxlength' => 30,
    '#weight' => 5,
    '#machine_name' => array(
      'exists' => 'entity_teaser_block_machine_name_exists',
      'source' => array('name'),
    ),
  );

  $form['actions'] = array(
    '#type' => 'actions'
  );
  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['#validate'][] = 'entity_teaser_block_clone_form_validate';
  $form['#submit'][] = 'entity_teaser_block_clone_form_submit';

  return $form;  
}

/**
 * Provide validate handler for block clone form.
 */
function entity_teaser_block_clone_form_validate( $form, &$form_state ) {

}

/**
 * Provide submit handler for block clone form.
 */
function entity_teaser_block_clone_form_submit( $form, &$form_state ) {
  $form_state['redirect'] = 'admin/structure/entity_teaser/block';
  
  $block = &$form_state['entity_teaser_block'];
  $block_new = clone $block;
  $value = &$form_state['input'];

  $block_new->setMachineName( $value['machine_name'] );
  $block_new->setName( $value['name'] );
  $block_new->setTitle( '' );
  $block_new->setType( '' );
  $block_new->setCid( 0 );
  $block_new->getConfiguration()->setConfigId( 0 );
  $block_new->save();
  drupal_set_message( t( 'Entity Teaser block @blockold has been clone to @blocknew successful.', array( '@blockold' => $block->getName(), '@blocknew' => $block_new->getName() ) ) );
}

/**
 * The entity_teaser block content editing form.
 */
function entity_teaser_block_content_form( $form, &$form_state, $block ) {
  $form = array();

  // load configuration of entity_teaser block
  $config = $block->getConfiguration();
  $form_state['entity_teaser_block'] = &$block;

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $block->getTitle(),
    '#maxlength' => 255,
  ); 

  $avail_types = $config->getTypesFull();

  $options = array();

  $form['avail_types'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#options' => &$options,
    '#default_value' => array( $block->getType() ),
  );

  foreach ($avail_types as $type => $data) {
    $options[$type] = $data['label'];

    $form['content_'.$type] = array(
      '#type' => 'textfield',
      '#title' => t('@type autocomplete', array( '@type' => $data['label'] ) ),
      '#autocomplete_path' => 'entity_teaser/block/'.$block->getMachineName().'/content/'.$type.'/autocomplete',
      '#states' => array(
        'visible' => array( ':input[name=avail_types]' => array('value' => $type ) ),
      ),
    );

    if( $type === $block->getType() ) {
      $id = $block->getCid();
      $entity_types = $block->getConfiguration()->getTypesFull();
      $entity_type = $entity_types[$type];
      $entities = entity_load( $type, array( $id ), array(), TRUE );
      if (array_key_exists($id, $entities)) {
        $entity = $entities[$id];
        $label = $entity->{ $entity_type[ 'entity keys' ][ 'label' ] };
        $default_value = _entity_teaser_block_content_identifier( $id , $label );
        $form['content_'.$type]['#default_value'] = $default_value;
      }
    }
  }

  $form['actions'] = array(
    '#type' => 'actions'
  );
  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['#validate'][] = 'entity_teaser_block_content_form_validate';
  $form['#submit'][] = 'entity_teaser_block_content_form_submit';

  return $form;

}

/**
 * Provide validate handler for block content editing form.
 */
function entity_teaser_block_content_form_validate( $form, &$form_state ) {
  $block = $form_state['entity_teaser_block'];
  $type = $form_state['input']['avail_types'];

  // Fetch the given content id in the content field of selected type.
  $cid = intval( _entity_teaser_block_content_parse_cid( $form_state['input']['content_'.$type] ) );
  // Check if the content id exitsts in types entities.
  if( !EntityTeaserBlock::isValidContent( $type, $cid ) ) {
    form_set_error( 'content_'.$type, t( "Content of type <em>@type</em> with id <em>@id</em> doesn't exists!", array( '@type' => $form['avail_types']['#options'][$type], '@id' => $cid ) ) );
  }
}

/**
 * Provide submit handler for block content editing form.
 */
function entity_teaser_block_content_form_submit( $form, &$form_state ) {
  $block = $form_state['entity_teaser_block'];
  // Check if a title is given.
  if( empty( $form_state['input']['title'] ) ) {
    // If title field in form is empty set block content title to an empty string.
    $block->setTitle( '' );
  } else {
    // If title is given, set these as the content block title.
    $block->setTitle( check_plain( $form_state['input']['title'] ) );
  }

  $type = $form_state['input']['avail_types'];
  // Fetch the given content id in the content field of selected type.
  $cid = intval( _entity_teaser_block_content_parse_cid( $form_state['input']['content_'.$type] ) );

  // Set entity_teaser block type and content id.
  $block->setType( $type );
  $block->setCid( $cid );

  // Save block in content mode.
  $block->save( ENTITY_TEASER_SAVE_MODE_CONTENT );

  drupal_set_message( t( 'Entity Teaser block has <em>@name</em> successful saved!', array( '@name' => $block->getName() ) ) );
}


/**
 * The autocomplete callback for entity_teaser block content fields.
 */
function entity_teaser_block_content_autocomplete( $block, $type, $str = '' ) {
  $entity_type = array();

  // Get all types which are defined for these block.
  $block_types = $block->getConfiguration()->getTypes();
  // Check if given type is available in entity_teaser block.
  if( !in_array( $type, $block_types ) ) {
    // Return empty json if type which is given via arguments is not available for these entity_teaser block.
    drupal_json_output(array());
    exit();
  } else {
    // Fetch all available types of entity_teaser block.
    $entity_types_full = $block->getConfiguration()->getTypesFull();
    // Get full informations of given type.
    $entity_type = $entity_types_full[$type];
  }
  
  // Create empty output variable.
  $var = array();

  // Define database alias by given entity type's base table.
  $alias = $entity_type['base table'];

  $query = db_select( $entity_type['base table'], $alias );
  $query->fields( $alias, array() );
  if( isset($entity_type['entity keys']['label']) && !empty($entity_type['entity keys']['label']) ) {
    // Select all records which contains the given autocomplete string in id or label column.
    $query->condition(
      db_or()
        ->condition( $alias.'.'.$entity_type['entity keys']['label'], '%'.db_like($str).'%', 'LIKE' )
        ->condition( $alias.'.'.$entity_type['entity keys']['id'], '%'.db_like($str).'%', 'LIKE' )
      );
  } else {
    $query->condition( $alias.'.'.$entity_type['entity keys']['id'], '%'.db_like($str).'%', 'LIKE' );
  }
  
  if ( isset( $entity_type[ 'entity keys' ][ 'bundle' ] ) ) {
    $bundles = array();
    foreach ( $entity_type['bundles'] as $type => $date ) {
      $bundles[] = $type;
    }
    if( !empty( $bundles ) ) {
      $query->condition( $alias.'.'.$entity_type['entity keys']['bundle'], $bundles, 'IN' );
    }
  }
  
  // Only show the first 5 records.
  $query->range(0,5);
  $result = $query->execute();
  // Fetch all records by assoziativ array of database result.
  // The records will attached to output variable.
  while( $record = $result->fetchAssoc() ) {
    $identifier = _entity_teaser_block_content_identifier( $record[$entity_type['entity keys']['id']], $record[$entity_type['entity keys']['label']] );
    $var[ $identifier ] = $identifier;
  }

  // Return output variable as json.
  drupal_json_output( $var );
  exit();
}

/**
 * Helper function to parse the content id from given string.
 */
function _entity_teaser_block_content_parse_cid( $value ) {
  // If regex matched fetch value from output.
  if( preg_match('/^(?<cid_digit>[0-9]+$)|(\[id:(?<cid_str>[0-9]+)\]\s(?<title>[A-Za-z0-9\s\S]+)$)/', $value, $out) ) {
    // Check if the regex return cid given by string or single integer.
    // If one of these available return these. If no one return false.
    if( isset( $out['cid_digit'] ) && !empty( $out['cid_digit'] ) ) {
      return $out['cid_digit'];
    } elseif( isset( $out['cid_str'] ) && !empty(  $out['cid_str']) ) {
      return $out['cid_str'];
    }else {
      return FALSE;
    }
  } else {
    return FALSE;
  }
}

/**
 * Helper function to return specific string for content fields in the content editing form.
 */
function _entity_teaser_block_content_identifier( $cid, $title ) {
  return '[id:'.$cid.'] '.$title;
}


/**
 * Provide a confirmation for delete block.
 */
function entity_teaser_block_delete_confirm( $form, &$form_state, $block ) {
  $form_state['entity_teaser_block'] = &$block;

  $question = t('Are you sure you want to delete the Entity Teaser block <em>@name</em>?', array( '@name' => $block->getName() ) );
  $path = 'admin/structure/entity_teaser/block';
  $description = t('This action cannot be undone.');
  $yes = t('Delete');

  return confirm_form( $form, $question, $path, $description, $yes );
}

/**
 * Submithandler for delete confirmation.
 */
function entity_teaser_block_delete_confirm_submit( $form, &$form_state ) {
  $form_state['redirect'] = 'admin/structure/entity_teaser/block';

  $block = &$form_state['entity_teaser_block'];
  $block->delete();

  drupal_set_message( t('Entity Teaser block <em>@name</em> is successful deleted.', array( '@name' => $block->getName() ) ) );
}
